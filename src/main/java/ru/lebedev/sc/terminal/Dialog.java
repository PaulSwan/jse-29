package ru.lebedev.sc.terminal;

import ru.lebedev.sc.service.SpecCalc;

import java.util.Scanner;

public class Dialog {

private  static  final SpecCalc specCalc = new SpecCalc();


    public static void main(String[] args) throws IllegalAccessException {

        final String SUM2NUM = "sum";
        final String FACTORIAL = "fac";
        final String FIBONACCI = "fib";
        final String MAX_VALUE = "max";

        Scanner scan = new Scanner(System.in);
        final String com = scan.nextLine();

        switch (com){
            case SUM2NUM: {
                System.out.println("[THE SUM OF TWO NUMBERS]");
                System.out.println("Please, enter first number:");
                final String num1 = scan.nextLine();
                System.out.println("Please, enter second number:");
                final String num2 = scan.nextLine();
                System.out.println("the sum is: " + specCalc.sum(num1,num2));
            } break;
            case FACTORIAL: {
                System.out.println("[FACTORIAL]");
                System.out.println("Please, enter a number:");
                final String num = scan.nextLine();
                System.out.println("Factorial " + num + " = " + specCalc.factorial(num));
            } break;
            case FIBONACCI: {
                System.out.println("[DECOMPOSITION NUMBER]");
                System.out.println("Please, enter a number:");
                final String num = scan.nextLine();
                long[] sumFib;
                sumFib = specCalc.fibonacci(num);
                System.out.println("Sum of Fibonacci numbers: ");
                for (long fibNum : sumFib) {
                    System.out.print(fibNum + " ");
                }
            } break;
            case MAX_VALUE: {
                System.out.println("max = " + Long.MAX_VALUE);
            }break;

            default: System.out.println("incorrect command");
            }
    }
}
