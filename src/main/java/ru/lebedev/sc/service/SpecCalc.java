package ru.lebedev.sc.service;


import java.util.ArrayList;

public class SpecCalc {
    public SpecCalc() {
    }

    public long sum(String arg1, String arg2) throws IllegalArgumentException{

        long result = toLong(arg1) + toLong(arg2);

        return result;
    }

    public  long factorial(String arg) throws IllegalArgumentException {
        long n = toLong(arg);
        if(n < 0) throw new IllegalArgumentException(n + "< 0");
        if (n == 0) {
            return 1L;
        }
        long fac = 1;
        for(int i = 1; i < n + 1; i++) {
            fac = multiply(fac, i);
        }
        return fac;
    }

       public long[] fibonacci(String arg) throws IllegalArgumentException {
        long num = toLong(arg);
        if(num < 0) throw new IllegalArgumentException(num + " is a negative number");
        ArrayList<Long> fibArray = new ArrayList<>();
        int index = 0;
        long sum = 0;
        do {
            if (index == 0){
                fibArray.add(0L);
                if (num == 0){
                    break;
                }
                index++;
                continue;
            }
            if(index == 1){
                fibArray.add(1L);
                if (num == 1){
                    break;
                }
                index++;
                continue;
            }

            fibArray.add(fibArray.get(index - 2) + fibArray.get(index -1));
            index++;

            sum = 0;
            for(long n: fibArray){
                sum += n;
            }
            if(sum > num) {
                throw new IllegalArgumentException(num + " cannot be decomposed into the sum of Fibonacci numbers");
            }

        } while(sum < num);

        index = fibArray.size();
        long[] result = new long[index];
        for(int i = 0; i < index; i++){
            result[i] = fibArray.get(i);
        }
        return result;

    }



    private long toLong(String arg) throws IllegalArgumentException {
        long result;
        try {
            result = Long.parseLong(arg);
        }
        catch (NumberFormatException | NullPointerException e){
            throw new IllegalArgumentException(e.getMessage());
        }
        return result;
    }


    private long multiply(Long val1, int val2){
        long result;
        try {
            result = Math.multiplyExact(val1,val2);
        }
        catch (ArithmeticException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
        return result;
    }

}
