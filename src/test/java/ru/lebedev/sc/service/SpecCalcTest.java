package ru.lebedev.sc.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class SpecCalcTest {
    SpecCalc specCalc;

    @BeforeEach
    private void setup(){
        specCalc = new SpecCalc();
    }

    @Test
    void sumCorrect() {
        assertEquals(5, specCalc.sum("2","3"));
    }

    @Test
    void sumExceptionOne() {
        assertThrows(IllegalArgumentException.class, () -> specCalc.sum("1.5","5a"));
    }

    @Test
    void factorialCorrect()   {
        assertEquals(5040, specCalc.factorial("7"));
    }

    @Test
    void factorialCorrectZero()   {
        assertEquals(1, specCalc.factorial("0"));
    }
    @Test
    void factorialExceptionNotNumberArg() {
        assertThrows(IllegalArgumentException.class, () -> specCalc.factorial("5d6"));
    }
    @Test
    void factorialExceptionNegativeArg() {
        assertThrows(IllegalArgumentException.class, () -> specCalc.factorial("-5"));
    }
    @Test
    void factorialExceptionOverflow() {
        assertThrows(IllegalArgumentException.class, () -> specCalc.factorial("21"));
    }
    @Test
    void fibonacciCorrect()   {
        long[] expected = {0, 1, 1, 2, 3};
        assertArrayEquals(expected, specCalc.fibonacci("7"));
    }

    @Test
    void fibonacciCorrectZero()   {
        long[] expected = {0};
        assertArrayEquals(expected, specCalc.fibonacci("0"));
    }

    @Test
    void fibonacciCorrectOne()   {
        long[] expected = {0, 1};
        assertArrayEquals(expected, specCalc.fibonacci("1"));
    }

    @Test
    void fibonacciNumberNotExpand() {
        assertThrows(IllegalArgumentException.class, () -> specCalc.fibonacci("19"));
    }

    @Test
    void fibonacciNegativeNumber() {
        assertThrows(IllegalArgumentException.class, () -> specCalc.fibonacci("-4"));
    }

    @Test
    void fibonacciNotNumberArg() {
        assertThrows(IllegalArgumentException.class, () -> specCalc.fibonacci("b4"));
    }

}